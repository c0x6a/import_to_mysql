#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Import data from TXT to MySQL

Copyright (C) 2012
Licence BSD

Authors:
    Carlos Joel Delgado Pizarro <cj@carlosjoel.net>
"""

import MySQLdb
import sys
from datetime import datetime


def import_db(argv):
    """
    Table and TXT file should be the same name.
        ej: table1.txt and table1 in MySQL
    """

    # you should configure connection parameters here
    db = MySQLdb.connect(host="localhost",  # your host, usually localhost
                         user="",           # your username
                         passwd="",         # your password
                         db="")             # name of the data base

    f = open("%s.txt" % argv, "r")

    cur = db.cursor()
    line_number = 0

    for line in f.readlines():
        line_number += 1

        if line.strip():
            sql_query = 'INSERT INTO %s VALUES (' % argv
            ll = [p for p in line.split('|')]  # change the separator if needed
            end_qry = '");'

            for l in ll:
                # check if line has " " to denotate strings
                if l.find('"') >= 0:
                    sql_query += '%s,' % str(l)
                    end_qry = ');'
                else:
                    try:
                        int(l)
                        sql_query += '%s,' % str(l)
                    except:
                        sql_query += '"%s",' % str(l)

            sql_query = '%s%s' % (sql_query[:-(len(end_qry) - 1)], end_qry)

            try:
                cur.execute(sql_query)
                db.commit()
            except Exception as e:
                fe = open("%s_log.txt" % argv, "a")
                fe.write("[%s] %d: %s\n" % (datetime.now().strftime("%d/%m/%Y - %H:%M:%S"), line_number, str(e)))
                fe.close()
                print e
    f.close()


if __name__ == '__main__':
    """
    Execute as main function
    """
    print "Starting script to import data."
    argv = sys.argv[1]

    try:
        import_db(argv)
        print "\n\t Import finished."
    except  Exception as e:
        print e
